<?php

include('simple_html_dom.php');

class readFileIntoTable {

    private $items;

    public function __construct() {
        $html = file_get_html(xammpVhostsPath);
        $DOM = new DOMDocument;
        @$DOM->loadHTML($html);
        $this->items = $DOM->getElementsByTagName('virtualhost');
        $this->readFromFile();
    }

    private function readFromFile() {
        echo("<table class=\"table table-striped\" id=\"bootstrap-overrides\">");
        echo("<thead>");
        echo("<tr>");
        echo("<th scope=\"col\">#</th>");
        echo("<th scope=\"col\">" . SERVER_NAME . "</th>");
        echo("<th scope=\"col\">" . PROJECT_PATH . "</th>");
        echo("</tr>");
        echo("</thead>");
        echo("<tbody>");
        for ($i = 0; $i < $this->items->length; $i++) {
            $hostName = $this->items->item($i)->nodeValue;
            //list hozzárendeli változókhoz a szeparált értékeket.
            list($DocumentRoot, $projectPath, $tmpServerName, $serverName) = explode('"', $hostName);
            /* @var $tmpServerName type */
            $tmpServerName = str_replace("ServerName", "", $tmpServerName);
            $projectPath = str_replace("c:/xampp", ".", $projectPath);
            echo("<tr>");
            echo("<th scope=\"row\">" . ($i + 1) . "</th>");
            echo("<td>" . $tmpServerName . "</td>" . "\r\n" . "<td>" . $projectPath . "<button type=\"button\" name = \"delBtn_" . $i . "\" class=\"close\">&times</button>" . "</td>" );
            echo("</tr>");
        }
        echo("</tbody>");
        echo("</table>");
        
    }
    
    public function test(){
        echo($this->items->item(0));
    }

}
