
<?php

@header('Content-type: text/html; charset=utf-8');
include('constants.php');
include('smarty/Smarty.class.php');
include('addNewElement/AppendIntoFile.php');
include('readFromFile/readFileIntoTable.php');


//index page 
$smarty = new Smarty();
$smarty->display('templates/index.tpl');

if (isset($_POST['submit'])) {
    $hostName = $_POST['hostName'];
    $dirPath = $_POST['dirPath'];
    if ($hostName != "" && $dirPath != "") {
        $object = new AppendIntoFile($hostName, $dirPath);
    } else {
        include("alerts/alert.php");
    }
}

$objectFileRead = new readFileIntoTable();
//$objectFileRead->test();



