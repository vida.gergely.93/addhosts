
<?php

include('constants.php');

class AppendIntoFile {
    
    public function __construct($hostName, $folderName) {
        $this->writeToFile(winHostPath, $this->winHostText($hostName));
        //You need full root permission to modify this file 
        $this->writeToFile(xammpVhostsPath, $this->vhostsText($hostName, $folderName));
         echo("The modification was successfully");
    }

    private function winHostText($hostName) {
        $szoveg = ("127.0.0.1 \t \t" . $hostName . ".localhost \r\n");
        return $szoveg;
    }

    private function vhostsText($hostName, $projektName) {
        $szoveg = "\r\n \t<VirtualHost *:80> \r\n" .
                "\tDocumentRoot \"c:/xampp/htdocs/" . $projektName . "/\"\r\n" .
                "\tServerName " . $hostName . ".localhost \r\n" .
                "\t<Directory \"c:/xampp/htdocs/" . $projektName . "/\">\r\n" .
                "\t</Directory> \r\n" .
                "\t</VirtualHost> \r\n";
        return $szoveg;
    }

    private function writeToFile($filePath, $pasteContent) {
        if (file_exists($filePath)) {
            file_put_contents($filePath, $pasteContent, FILE_APPEND | LOCK_EX);
        } else {
            $dir = dirname($filePath);
            echo(" The file doesn't exists \n The following path: " . $dir . "<br>");
            echo("The file append was not successfully");
        }
    }

}
